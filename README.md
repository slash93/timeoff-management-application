# To the Interviewers

Hi, this project is not completed. But I will like to share what was accomplish.

## Architectural Diagram

[INCOMPLETED]

## Repository
GitLab is the repository used for both git and docker images.

https://gitlab.com/slash93/timeoff-management-application

## CI/CD
Jenkins server running on aws ec2 free tier instance.

http://ec2-34-229-127-76.compute-1.amazonaws.com:8080/

## Artifact Management
[INCOMPLETE] Artifactory to be used.


## Infrastructure as Code
[INCOMPLETE] Ansible is not used. However terraform is used.

## Application Hosting
App is hosted in a AWS EC2 AMI Linux instance. 

http://ec2-34-201-61-131.compute-1.amazonaws.com:3000

(Public DNS will change)

