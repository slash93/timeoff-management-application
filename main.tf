variable "aws_key_pair" {
  default = "/aws/aws_keys/default-ec2.pem"
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_default_vpc" "default" {

}

data "aws_subnet_ids" "default_subnets" {
  vpc_id = aws_default_vpc.default.id
}

resource "aws_security_group" "http_server_sg" {
  name   = "http_server_sg"
  vpc_id = aws_default_vpc.default.id

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
  }

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 3000
    protocol    = "tcp"
    to_port     = 3000
  }

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 0
    protocol    = -1
    to_port     = 0
  }

}

resource "aws_instance" "http_server" {
  ami                    = "ami-0742b4e673072066f"
  key_name               = "default-ec2"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.http_server_sg.id]
  subnet_id              = tolist(data.aws_subnet_ids.default_subnets.ids)[0]

  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ec2-user"
    private_key = file(var.aws_key_pair)
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum install -y gcc-c++ make",
      "curl -sL https://rpm.nodesource.com/setup_15.x | sudo -E bash -",
      "sudo yum install -y nodejs",
      "sudo yum update -y",
      "sudo yum install git -y",
      "echo ********start**************",
      "git clone https://gitlab.com/slash93/timeoff-management-application.git timeoff-management",
      "cd timeoff-management",
      "npm install -y",
      "npm start",
      "echo ********end**************"
    ]
  }

}